import sys

historia = []
komenda = []

while True:
    wejscie = input()
    if wejscie == "":
        if komenda:
            historia.append(komenda)
        break
    elif wejscie in ["saldo", "zakup" , "sprzedaz"]:
        if not komenda:
            komenda.append(wejscie)
        else:
            historia.append(komenda)
            komenda = [wejscie]
    else:
        komenda.append(wejscie)

suma = 0
magazyn = {}

for komenda in historia:
    if komenda[0] == "saldo":
        suma += int(komenda[1])
    elif komenda[0] == "zakup":
        suma -= int(komenda[2])*int(komenda[3])
        if komenda[1] in magazyn.keys():
            magazyn[komenda[1]] += int(komenda[3])
        else:
            magazyn[komenda[1]] = int(komenda[3])
        if suma < 0 or int(komenda[2]) < 0 or int(komenda[3]) < 0:
            break
    elif komenda[0] == "sprzedaz":
        suma += int(komenda[2])*int(komenda[3])
        if komenda[1] in magazyn.keys():
            if magazyn[komenda[1]] >= int(komenda[3]):
                magazyn[komenda[1]] -= int(komenda[3])
        else:
            print("Nie da sie sprzedac")
            break

        if int(komenda[2]) < 0 or int(komenda[3]) < 0:
            break
    else:
        print("blad")
        break

if sys.argv[1] != "konto" and sys.argv[1] != "przeglad" and sys.argv[1] != "magazyn":
    komenda = sys.argv[1:]
    historia.append(komenda)

if sys.argv[1] == "saldo" and (suma + int(sys.argv[2])) >= 0:
    suma += int(sys.argv[2])

elif sys.argv[1] == "zakup":
    suma -= int(sys.argv[3])*int(sys.argv[4])
    if sys.argv[2] in magazyn.keys():
        magazyn[sys.argv[2]] += int(sys.argv[4])
    else:
        magazyn[sys.argv[2]] = int(sys.argv[4])
    if suma < 0 or int(sys.argv[3]) < 0 or int(sys.argv[4]) < 0:
        print("blad")

elif sys.argv[1] == "sprzedaz":
    suma += int(sys.argv[3])*int(sys.argv[4])
    if sys.argv[2] in magazyn.keys() and magazyn[sys.argv[2]] >= int(sys.argv[4]):
            magazyn[sys.argv[2]] -= int(sys.argv[4])
    else:
        print("nie da sie sprzedac")

    if int(sys.argv[3]) < 0 or int(sys.argv[4]) < 0:
        print("blad")

elif sys.argv[1] == "konto":
    print("Saldo wynosi: ", suma)

elif sys.argv[1] == "magazyn":
    print("Stan magazynu: ", magazyn)

elif sys.argv[1] == "przeglad":
    print("Przeglad: ", historia)
else:
    print("blad")

if sys.argv[1] in ['saldo', 'zakup', 'sprzedaz']:
    for komenda in historia:
        for linijka in komenda:
            print(linijka)
    print("")
